# génial-openstreetmap

Une liste organisée d'outils géniaux autour de OSM
## Table of Contents
- [Éditeurs](#éditeurs)
    - [PC](#pc)
    - [Android](#android)
- [Rendus](#rendus)
    - [Généralistes](#généralistes)
    - [Spécialisés](#spécialisés)
- [Assurance qualité et changesets](#assurance-qualité-et-changesets)
## Éditeurs
### PC
- [iD](http://www.openstreetmap.org/edit?editor=id) Éditeur simple pour navigateur web.
- [JOSM](https://josm.openstreetmap.de) Éditeur complet pour PC.
### Android
- [Vespucci](https://vespucci.io/) Éditeur pour Android.
## Rendus
### Généralistes
- [osmfr](http://tile.openstreetmap.fr/) Rendus OpenStreetMap France.
- [CyclOSM](https://www.cyclosm.org/) Rendu pour la pratique du vélo.
- [OpenSeaMap](http://www.openseamap.org/) Rendu maritime.
- [Hike & Bike Map](http://hikebikemap.org/) Rendu pour la randonnée.
- [OpenTopoMap](https://opentopomap.org/) Rendu topographique.
- [F4map Démo](http://demo.f4map.com) Visualisation en 3D des données OSM.
- [OpenLevelUp!](https://openlevelup.net/) Visualisation de la cartographie d'intérieur de OSM.
- [Map Compare](http://tools.geofabrik.de/mc) Comparateur de rendus généralistes.
- [OpenWhateverMap](http://openwhatevermap.xyz/) Démo de rendus divers.
- [Show Me The Way](http://osmlab.github.io/show-me-the-way/) Une visualisation des dernières contributions OSM dans le monde.
### Spécialisés
- [OpenFireMap](http://openfiremap.org/) Carte des équiments de lutte contre les incendis.
- [OsmHydrant](https://www.osmhydrant.org/) Carte éditable pour la lutte contre les incendis.
- [Wheelmap](https://wheelmap.org/) Carte éditable concernant l'accessibilité en fauteuil roulant. 
- [OpenRailwayMap](https://www.openrailwaymap.org/) Rendu des réseaux ferroviaires.
- [MapContrib](https://www.mapcontrib.xyz/) Vaste choix de cartes thématiques permettant également de contribuer.
- [OpenFuelMap](https://openfuelmap.net/) Cartes des stations essences et des prix.
- [ÖPNVKarte](https://www.xn--pnvkarte-m4a.de/) Carte des réseaux de transports publics.
- [Open Infrastructure Map](https://openinframap.org/) Carte des réseaux d'infrastructure.
- [Surveillance under Surveillance](https://sunders.uber.space/) Carte éditable des équipements de surveillance (caméras et guardes).
- [Historical Objects](http://gk.historic.place) Carte des monuments historiques.
- [European Navigable Waterways](http://maps.grade.de/) Carte des voies d'eau navigables en Europe.
- [Babykarte](https://babykarte.openstreetmap.de/) Carte des aménagements pour la petite enfance.
## Assurance qualité et changesets
- [OSM History Viewer](https://osmhv.openstreetmap.de/) Analyseur de changeset et de relations.
- [achavi](https://overpass-api.de/achavi/?changeset=55615474) Visualisation graphique des modifications d'un changeset.
- [OSMCha](https://osmcha.mapbox.com/) Recherche, surveillance et analyse de changesets. Très complet.
- [OSM Relation Analyzer](http://ra.osmsurround.org) Analyseur de relation, principalement pour les type route et boundary (continuité).
- [osm qa feeds](https://tyrasd.github.io/osm-qa-feeds/) Générateur de flux RSS pour une zone donnée.
- [WHODIDIT](https://simon04.dev.openstreetmap.org/whodidit/) Visualisation sur une carte des changesets récents.
- [KeepRight!](https://www.keepright.at/) Analyses de cohérence des données, principalement pour la navigation.
- [Osmose QA](http://osmose.openstreetmap.fr) Nombreuses analyses de cohérence et intégration de données, notamment en France. Très complet.
- [OSM Inspector](https://tools.geofabrik.de/osmi/) Analyses de cohérence des données par thème.
- [OpenstreetMap Truck QA Map](http://maxheight.bplaced.net) Outil spécialisé dans les limites des routes : largeur, longueur, poids des véhicules.
- [Layers OSM France](http://layers.openstreetmap.fr/) Diverses analyses proposées par OpenStreetMap France.
## Licence
[![CC0](http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/cc-zero.svg)](https://creativecommons.org/publicdomain/zero/1.0/)